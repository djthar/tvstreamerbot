import sys
import asyncio
import telepot
import telepot.aio
import subprocess
import pprint
from pathlib import Path


pp = pprint.PrettyPrinter(indent=4)

"""
$ python3 tvstreamerbot.py <token> <username>
A bot to control the UDPXY server.
"""


TOKEN = sys.argv[1]  # get token from command-line
allowed_usernames = [sys.argv[2]]
default_auth_file_name = 'auth_file'


class DummySubprocess():
    def terminate(self):
        pass

    def kill(self):
        pass

    def poll(self):
        return 0


class UDPXYServer():
    def __init__(self):
        self._udpxy_process = DummySubprocess()
        self._udpxy_auth_file_name = default_auth_file_name

    def is_started(self):
        return self._udpxy_process.poll() is None

    def start(self, auth_file_name):
        self._udpxy_auth_file_name = auth_file_name
        cmd = 'docker run -p 4022:4022 dorftv/udpxy'.split()
        self._udpxy_process = subprocess.Popen(cmd)

    def stop(self):
        self._udpxy_process.terminate()

    def kill(self):
        self._udpxy_process.kill()

    def get_auth_file_content(self):
        f = Path(self._udpxy_auth_file_name)
        if f.is_file():
            return f.read_text()
        else:
            return ''


udpxy_server = UDPXYServer()


class StartCommand():
    def __init__(self, server_auth_file_name = default_auth_file_name):
        global udpxy_server
        self._server_auth_file_name = server_auth_file_name
        if udpxy_server.is_started():
            self._is_already_started = True
            self._response_message = "UDPXY server is already started"
        else:
            self._is_already_started = False
            self._response_message = "UDPXY server is starting"

    async def run(self, bot, chat_id):
        global udpxy_server
        udpxy_server.start(self._server_auth_file_name)
        await bot.sendMessage(chat_id, self._response_message)


class StopCommand():
    async def run(self, bot, chat_id):
        global udpxy_server
        udpxy_server.stop()
        await bot.sendMessage(chat_id, "UDPXY server is stopping")


class KillCommand():
    async def run(self, bot, chat_id):
        global udpxy_server
        udpxy_server.kill()
        await bot.sendMessage(chat_id, "UDPXY server is killed")


class StatusCommand():
    async def run(self, bot, chat_id):
        global udpxy_server
        await bot.sendMessage(chat_id, "UDPXY server is {}".format("running" if udpxy_server.is_started() else "stopped"))


class ShowServerCredentials():
    async def run(self, bot, chat_id):
        global udpxy_server
        await bot.sendMessage(chat_id, udpxy_server.get_auth_file_content())


class DummyCommand():
    async def run(self, bot, chat_id):
        pass


class ServerCommandBuilder():
    COMMAND = {'/start': StartCommand,
               '/stop': StopCommand,
               '/kill': KillCommand,
               '/status': StatusCommand,
               '/credentials': ShowServerCredentials,
               }

    def __init__(self, msg):
        self._msg = msg
        if self._is_a_command():
            self._init_as_a_command()
        else:
            self._correct = False

    def _is_a_command(self):
        return self._contains_entities() and self._has_only_one_entitie() and self._is_first_entitie_a_bot_command()

    def _contains_entities(self):
        return 'entities' in self._msg

    def _has_only_one_entitie(self):
        return len(self._msg['entities']) == 1

    def _is_first_entitie_a_bot_command(self):
        return self._msg['entities'][0]['type'] == 'bot_command'

    def _init_as_a_command(self):
        msg_elements = self._msg['text'].split()
        self._command = msg_elements[0]
        self._command_arguments = msg_elements[1:]
        self._correct = True

    def build(self):
        if self._correct and self._command in self.COMMAND:
            try:
                return self.COMMAND[self._command](*self._command_arguments)
            except TypeError as e:
                return DummyCommand()
        else:
            return DummyCommand()


class TvStreamerBot(telepot.aio.Bot):
    async def on_chat_message(self, msg):
        global allowed_usernames
        content_type, chat_type, chat_id = telepot.glance(msg)

        r = await self.getChat(chat_id)
        if not r['username'] in allowed_usernames:
            return
        await self._handle_msg(msg)

    async def _handle_msg(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        command = ServerCommandBuilder(msg).build()
        await command.run(self, chat_id)
        pp.pprint(msg)


bot = TvStreamerBot(TOKEN)
loop = asyncio.get_event_loop()
#loop.set_debug(True)

loop.create_task(bot.message_loop())
print('Listening ...')

loop.run_forever()

